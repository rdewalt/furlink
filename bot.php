<?php
require_once("config.inc");
require_once("lib.inc");
require_once("help.inc");

L("Furlink 2016 Initializing...");

L("Initializing Connection");
$socket = socket_create( AF_INET, SOCK_STREAM, SOL_TCP ) ;
$error = socket_connect ($socket, $server, $port);
if ($socket === false ) {
    $errorCode=socket_last_error();
    $errorString=socket_strerror($errorCode);
    L("Error: $errorCode: $errorString");
    die("Error: $errorCode: $errorString");
}

socket_write ($socket, "NICK $nickname\r\n");
socket_write ($socket, "USER $ident * 8 : $gecos\r\n");

// MAIN LOOP
while ( is_resource ( $socket ) ) {
$data = trim( socket_read( $socket, 1024, PHP_NORMAL_READ ) );

//dump the raw data to the log... because.
L($data);

$d= explode(' ', $data);
$d = array_pad($d,10,'');

if ($d[0]==='PING') { socket_write ($socket, 'PONG ' . $d[1]. "\r\n"); } 

//PRIVMSG (/msg or /query) HANDLER
if ($d[1]==='PRIVMSG') { 
        $user=GrabUser($d[0]);
        L("UserMessage from - ".$user[0]);
        switch(strtolower($d[3])) {
//HELP
        case ':help':
        BotSay($user[0],"Oh crap, Arty hasn't written the help file yet.");
        break;
//SHUTDOWN
        case ':shutdown':
        ShutdownBot($user[0]);
        break;       
//REGISTER
        case ':register':
        RegisterUser($user[0],$d[4],$user[1]);
        break;       
//AUTHENTICATE
        case ':auth':
        case ':authenticate':
        AuthenticateUser($user[0],$d[4],$user[1]);
        break;       
//DEFAULT
        default:
        BotSay($user[0],"Oh crap, Arty hasn't written a handler for that yet.");
        break;

        }
    }

}
// END MAIN LOOP

L("===Shutting Down===\n\n");
?>
