<?php
require_once("lib.inc");

function GetDescription($user,$target)
{   global $dbh;
    $query="select Description from descriptions where Name = ?";
    L("GetDesc: target: $target");
    BotSay($user, "Desc for $target:");
    $sth= $dbh->prepare($query);
    $sth->bind_param("s",$target);
    $sth->bind_result($desc);
    $sth->execute();
    while ($sth->fetch() ) {
        BotSay($user, $desc);
    }
    BotSay($user, "End Description");
}

function SetDesc($User,$Host,$newDesc)
{
    if(IsUserAuthenticated($User,$Host)) {
    global $dbh;    
    $query="delete from descriptions where Name = ?";
    L("SetDesc: $User");
    $sth= $dbh->prepare($query);
    $sth->bind_param("s",$User);
    $sth->execute();

    $query="insert into descriptions values ( ? ,now(), ? )";
    $sth= $dbh->prepare($query);
    $sth->bind_param("ss",$User,implode(" ",$newDesc));
    $sth->execute();
    GetDescription($User, $User); // Re-display the user's desc after setting it.
    }  else {
    BotSay($User,"Authentication Failed: Please re-authorize with \"auth <password>\".");
    }
}

function AppendDesc($User,$Host,$newDesc)
{
    if(IsUserAuthenticated($User,$Host)) {
    global $dbh;    
    L("AppendDesc: $User");
    $query="insert into descriptions values ( ? ,now(), ? )";
    $sth= $dbh->prepare($query);
    $sth->bind_param("ss",$User,implode(" ",$newDesc));
    $sth->execute();
    GetDescription($User, $User); // Re-display the user's desc after setting it.
    }  else {
    BotSay($User,"Authentication Failed: Please re-authorize with \"auth <password>\".");
    }
}

function DescriptionHandler($User,$data)
{   $U=$User[0];$Host=$User[1];
    $newDesc=array_slice($data, 5);
    switch( strtolower($data[4]) ) {
        case 'show':
        GetDescription($U,$newDesc[0]);
        break;
        case 'set':
        SetDesc($U,$Host,$newDesc);
        break;
        case 'append':
        AppendDesc($U,$Host,$newDesc);
        break;
        default:
        BotSay($User,"I don't understand what you meant.");
        break;
    }
}
