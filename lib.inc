<?php
// Main Library stack,

  $AdminGroup = array ('artisan'=>True);

// Generic "Log to Console/Log to File function"
function L($String)
{
$CONSOLE=0;
$LOGFILE=1;
$DateStamp=date("Y-m-d H:i:s T");
  if ($CONSOLE==1) { echo "$DateStamp - $String\n"; }
  if ($LOGFILE==1) {
    $file=fopen("bot.log",'a');
    fwrite($file,"$DateStamp - $String\n");
    fclose($file);
  } 
}

function IsAdmin($user)
{
    global $AdminGroup;
    return isset($AdminGroup[strtolower($user)]);
}

function StartupBot()
{
    L("Wiping Authentication Table");
    $query="truncate AuthenticatedUsers";
    $sth= $dbh->prepare($query);

// Lets not wipe it just yet.

///    $sth->execute();
}

function ShutdownBot($User)
{   global $socket;
    if (IsAdmin($User)) {
    L("Shutting Down!!!");
    BotSay($User,"Oh, okay... Shutting Down.");
    socket_write ($socket, "QUIT Shutting Down!");
    socket_close($socket); 
    die("Normal Shutdown");
    } else {
    L("Shutdown Attempt Defeated!");
    BotSay($User,"How about.... nope.");
    }
}


function MakePassword($p)
{  // One common password function, so I can fuck it up later good and properly.
    $password=password_hash($p, PASSWORD_DEFAULT); 
    return $password;
}

function GrabUser($UserString)
{ //Convert :Artisan!ryan@I.broke.services.again.Id.bet to proper format
$User=explode("!",$UserString);
$User[0]=str_replace(":","",$User[0]);
return $User;
}

function BotSay($user,$message)
{  global $socket;
 socket_write ($socket, "PRIVMSG $user $message\r\n"); 
}


//AuthenticatedUsers is an engine=MEMORY table, if I restart the bot, I want everyone to have to re-auth to it.

function AuthenticateUser($user, $password, $host)
{
    L("Authenticate User: $user - $host"); 
    $U=GetAuthUserFromDB($user);
    
    if (password_verify($password,$U['Password']) && $U['Name']===$user) {
    global $dbh;
    L("AuthUser: $user $host");
    $query="insert into AuthenticatedUsers values ( ? , ? , now()) on Duplicate Key update T=now()";
    $sth= $dbh->prepare($query);
    $sth->bind_param("ss",$user,$host);
    $sth->execute();
    BotSay($user,"Authentication Successful!");
    } else {
    // YOU SHAL NOT PASS...er DID NOT PASS
    L("Authentication Error: $user $host $password");
    $DateStamp=date("Y-m-d H:i:s T");
    BotSay($user,"Authentication Failed: Perhaps wrong password, perhaps wrong name, perhaps something fucky... Make sure you are using \"authenticate <password>\"  /msg Artisan when he's on. Mention timestamp: $DateStamp");
    }
}

function RegisterUser($user, $password,$host)
{
$U=GetAuthUserFromDB($user);
if ($U['Name']===$user) {
    L("Registration Error: $user");
    $DateStamp=date("Y-m-d H:i:s T");
    BotSay($user,"You have already been registered.  Please Consider the 'authenticate' rather than the 'register' option.  If you think this can't possibly be the case, /msg Artisan when he's on. Mention timestamp: $DateStamp");
    }
  else
    {
    $password=MakePassword($password); 
    WriteAuthUserToDB($user,$password,$host); 
   // Verify it got there.
   $U=GetAuthUserFromDB($user);  
    if ($U['Name']===$user) {
    BotSay($user, "Registration Successful.");
    } else { 
    $DateStamp=date("Y-m-d H:i:s T");
    BotSay($user, "Shit... somethings fucky...  /msg Artisan when he's on. Mention timestamp: $DateStamp");
    }
    }
}

function GetAuthUserFromDB($user)
{   global $dbh;
    $query="select Name,Hostname,Password from Authenticate where Name= ?";
    L("GetAuthUserFromDB: $query -- $user");
    $sth= $dbh->prepare($query);
    $sth->bind_param("s",$user);
    $sth->bind_result($N,$H,$P);
    $sth->execute();
    $sth->fetch();
    $row['Name']=$N;
    $row['Hostname']=$H;
    $row['Password']=$P;
    return $row;
}

// I think the reason I'm storing "host" is for auditing, or I think I'm smart.
// Password should be encrypted here.  So thats okay for me to log it perhaps.
function WriteAuthUserToDB($user,$password,$host)
{   global $dbh;
    L("WriteAuthUserToDB: $user $password $host");
    $query="insert into Authenticate values (? , ? , ?)"; 
    $sth= $dbh->prepare($query);
    $sth->bind_param("sss",$user,$host,$password);
    $sth->execute();
}

function IsUserAuthenticated($user,$host)
{   global $dbh;
    $query="select Name from AuthenticatedUsers where Name= ? and Host= ?"; 
    L("AuthenticateCheck: $host -- $user");
    $sth= $dbh->prepare($query);
    $sth->bind_param("ss",$user, $host);
    $sth->bind_result($N);
    $sth->execute();
    $sth->fetch();
    if ($N===$user) {
    L("Authentication Passed");
     return True; 
    } else { 
    L("Authentication Failed");
    return False; 
    }
}
