This is the repository of my Furlink bot.   Originally, "Furlink" was a bot that
did very basic IRC things.  It was written by someone else, but I had legit
acquired a copy of the code to extend and use.

This was 10 years or more ago.  The original was written in perl.

I've taken to re-write it, or at least, write a new one inspired by it.

Furlink was orignally a "description bot"  among other features.  I guess you
had to be there to understand things.

I've written it in PHP.. because... um...  I don't have a reason really.  I 
could have easily written this in Python or Perl, but lets be honest here, no
matter what language I write it in, someone will say "Why didn't you just 
use <other lang>?"  So...  

"Well maybe he hates himself, perhaps thats why he did it in php..."   Honestly?
if I wanted to hate myself with code, I'd write it in C or something worse. I'm
fluent enough in php that other than having to learn one or two things, I only
had to re-learn the IRC Protocol.  Which was rather easy. (RFC 1459)

I'm posting this to gitlab because that is where I store my projects.  Fork and
enjoy.
