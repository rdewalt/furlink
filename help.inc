<?php
//  Bot Help File

require_once("lib.inc");

function ShowHelp($user,$stack)
{
    BotSay($user,"Help, or at least an attempt at help;");
    BotSay($user,"Furlink 2016 v1.0");
    BotSay($user,"Commands::");
    BotSay($user,". ");
    BotSay($user,"register <password> -- Register with the bot.");
    BotSay($user,"auth <password> -- Authenticate witht the bot (needed for things)");
    BotSay($user,". ");
    BotSay($user,"desc show <username> -- Show a user's desc");
    BotSay($user,"desc set <description> -- Set your description");
    BotSay($user,"desc append <description>  -- For using more than one line.");
    BotSay($user,". ");
    BotSay($user,"There will be more when Arty gets around to writing them in.  Let him know suggestions for things you want the bot to do. ");
}
